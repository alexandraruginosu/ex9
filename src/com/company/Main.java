package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        Set<String> myEmployeesSet1 = new HashSet<>();

        myEmployeesSet1.add("Samuel Smith");
        myEmployeesSet1.add("Bill Williams");
        myEmployeesSet1.add("Anthony Johnson");
        myEmployeesSet1.add("Cartner Caragher");

        System.out.println("myEmployeesSet1: " + myEmployeesSet1);

        List<String> list = new ArrayList();

        list.add("Will Smith");
        list.add("Benjamin McCartney");
        list.add("Albert Davidson");
        list.add("Jaden Clinton");
        list.add("Donald Lincoln");
        list.add("Bart Bureau");
        list.add("Alistaire O'Doherty");

        Set<String> myEmployeesSet2 = new HashSet<>(list);

        System.out.println("List : " + list);
        System.out.println("myEmployeesSet2: " + myEmployeesSet2);

        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + myEmployeesSet1.equals(myEmployeesSet2));

        myEmployeesSet2.remove("Alistaire O'Doherty");

        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + myEmployeesSet1.equals(myEmployeesSet2));

        System.out.println("myEmployeesSet1 contains all elements: " + myEmployeesSet1.containsAll(list));

        System.out.println("myEmployeesSet1 contains all elements: " + myEmployeesSet2.containsAll(list));

        Iterator<String> it = myEmployeesSet1.iterator();

        while (it.hasNext()) {
            System.out.println("iterator loop: " + it.next());
        }

        myEmployeesSet1.clear();
        System.out.println("myEmployeesSet1 is Empty: " + myEmployeesSet1.isEmpty());

        System.out.println("myEmployeesSet1 has: " + myEmployeesSet1.size() + " elements.");

        System.out.println("myEmployeesSet2 has: " + myEmployeesSet2.size() + " elements.");


        String[] the_array = myEmployeesSet2.toArray(new String[myEmployeesSet2.size()]);

        System.out.println("The array: " + Arrays.toString(the_array));
    }
}
